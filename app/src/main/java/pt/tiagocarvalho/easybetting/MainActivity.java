package pt.tiagocarvalho.easybetting;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pt.tiagocarvalho.easybetting.adapter.MatchesAdapter;
import pt.tiagocarvalho.easybetting.entity.Game;
import pt.tiagocarvalho.easybetting.network.AsyncTaskListResult;
import pt.tiagocarvalho.easybetting.network.AsyncTaskLoaderListener;
import pt.tiagocarvalho.easybetting.network.GamesAsyncTakLoader;
import pt.tiagocarvalho.easybetting.utils.PrefUtils;
import pt.tiagocarvalho.easybetting.workers.FindMatchesWorker;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static androidx.work.ExistingPeriodicWorkPolicy.REPLACE;
import static butterknife.ButterKnife.bind;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.math.BigDecimal.ZERO;
import static java.util.concurrent.TimeUnit.MINUTES;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getBooleanFromKey;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getIntFromKey;
import static pt.tiagocarvalho.easybetting.utils.Utils.isNetworkUnavailable;
import static pt.tiagocarvalho.easybetting.utils.Utils.setMenuItemActiveString;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SwipeRefreshLayout.OnRefreshListener,
        LoaderManager.LoaderCallbacks<AsyncTaskListResult<Game>>,
        MatchesAdapter.MatchesAdapterOnClickHandler,
        AsyncTaskLoaderListener {

    public static final String EXTRA_MATCH_URL = "pt.tiagocarvalho.easybetting.extra.game.url";
    public static final Integer RATIO_ASC = 0;
    public static final Integer RATIO_DESC = 1;
    public static final Integer DATE_ASC = 2;
    public static final Integer DATE_DESC = 3;
    public static final String PREF_SCANNED_COUNT = "pt.tiagocarvalho.easybetting.scanned";
    public static final String PREF_COMBINATIONS_COUNT = "pt.tiagocarvalho.easybetting.combinations";
    private static final int DEFAULT_NUMBER_COLUMNS = 1;
    private static final String FIND_MATCHES_UPDATE_WORK = "FIND_MATCHES_UPDATE_WORK";
    private static final int MATCHES_LOADER_ID = 454545;
    private static final String KEY_FILTER_WIN_GUARANTEE = "pt.tiagocarvalho.easybetting.filter.variation";
    private static final String KEY_FILTER_TODAY = "pt.tiagocarvalho.easybetting.filter.today";
    private static final String KEY_FILTER_OWNED = "pt.tiagocarvalho.easybetting.filter.owned";
    private static final String KEY_SORT_ORDER = "pt.tiagocarvalho.easybetting.sort.order";
    private static final String KEY_SHOW_ODDS = "pt.tiagocarvalho.easybetting.show.odds";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.rvMatches)
    RecyclerView rvMatches;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tvError)
    TextView tvError;
    @BindView(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;

    private FirebaseAnalytics mFirebaseAnalytics;

    private MatchesAdapter matchesAdapter;
    private boolean winGuarantee;
    private boolean showOdds;
    private boolean today;
    private boolean owned;
    private int sortOrderId;
    private long executionTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind(this);
        Timber.plant(new Timber.DebugTree());
        setSupportActionBar(toolbar);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Easy Betting");
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        matchesAdapter = new MatchesAdapter(null, MainActivity.this, this);
        rvMatches.setAdapter(matchesAdapter);
        rvMatches.setHasFixedSize(true);
        rvMatches.setLayoutManager(new GridLayoutManager(this, DEFAULT_NUMBER_COLUMNS));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.colorPrimaryLight),
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimaryText),
                getResources().getColor(R.color.colorSecondaryText));

        getSortAndFilterSettings();
        setupWorkers();
        restartLoaderIfNetworkAvailable(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_matches, menu);

        menu.findItem(R.id.menu_action_matches_filter_win).setChecked(winGuarantee);
        menu.findItem(R.id.menu_action_matches_filter_today).setChecked(today);
        menu.findItem(R.id.menu_action_matches_filter_owned).setChecked(owned);
        menu.findItem(R.id.menu_action_matches_show_odds).setChecked(showOdds);

        MenuItem sortRatioItem = menu.findItem(R.id.menu_action_matches_sort_ratio);
        sortRatioItem.setTitle("Ratio");
        MenuItem sortDateItem = menu.findItem(R.id.menu_action_matches_sort_date);
        sortDateItem.setTitle("Date");

        if (sortOrderId == RATIO_ASC) {
            setMenuItemActiveString(sortRatioItem, "Ratio", "▲");
        } else if (sortOrderId == RATIO_DESC) {
            setMenuItemActiveString(sortRatioItem, "Ratio", "▼");
        } else if (sortOrderId == DATE_ASC) {
            setMenuItemActiveString(sortDateItem, "Date", "▲");
        } else if (sortOrderId == DATE_DESC) {
            setMenuItemActiveString(sortDateItem, "Date", "▼");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_action_matches_filter_win:
                winGuarantee = !winGuarantee;
                changeFilter(KEY_FILTER_WIN_GUARANTEE, winGuarantee);
                return true;
            case R.id.menu_action_matches_filter_today:
                today = !today;
                changeFilter(KEY_FILTER_TODAY, today);
                return true;
            case R.id.menu_action_matches_filter_owned:
                owned = !owned;
                changeFilter(KEY_FILTER_OWNED, owned);
                return true;
            case R.id.menu_action_matches_sort_ratio:
                if (sortOrderId == RATIO_ASC) {
                    sortOrderId = RATIO_DESC;
                } else {
                    sortOrderId = RATIO_ASC;
                }
                changeSort();
                return true;
            case R.id.menu_action_matches_sort_date:
                if (sortOrderId == DATE_ASC) {
                    sortOrderId = DATE_DESC;
                } else {
                    sortOrderId = DATE_ASC;
                }
                changeSort();
                return true;
            case R.id.menu_action_matches_show_odds:
                showOdds = !showOdds;
                changeFilter(KEY_SHOW_ODDS, showOdds);
                return true;
            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void changeFilter(String key, boolean state) {
        // already start loading, do not need to wait on saving prefs
        restartLoaderIfNetworkAvailable(false);

        // save new setting
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(key, state).apply();

        // refresh filter icon state
        invalidateOptionsMenu();
    }

    private void changeSort() {
        // already start loading, do not need to wait on saving prefs
        restartLoaderIfNetworkAvailable(false);

        // save new sort order to preferences
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(KEY_SORT_ORDER, sortOrderId).apply();

        // refresh menu state to indicate current order
        invalidateOptionsMenu();
    }

    private void getSortAndFilterSettings() {
        winGuarantee = getBooleanFromKey(MainActivity.this, KEY_FILTER_WIN_GUARANTEE, TRUE);
        showOdds = getBooleanFromKey(MainActivity.this, KEY_SHOW_ODDS, FALSE);
        today = getBooleanFromKey(MainActivity.this, KEY_FILTER_TODAY, TRUE);
        owned = getBooleanFromKey(MainActivity.this, KEY_FILTER_OWNED, FALSE);
        sortOrderId = getIntFromKey(MainActivity.this, KEY_SORT_ORDER, RATIO_DESC);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_exchanges) {
            Intent intent = new Intent(MainActivity.this, ProvidersActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_calculator) {
            Intent intent = new Intent(MainActivity.this, CalculatorActivity.class);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void restartLoaderIfNetworkAvailable(boolean init) {
        if (isNetworkUnavailable(MainActivity.this)) {
            createErrorAlertWithText("No network available", "No network available");
            return;
        }

        if (init) {
            getSupportLoaderManager().initLoader(MATCHES_LOADER_ID, null, this);
        } else {
            getSupportLoaderManager().restartLoader(MATCHES_LOADER_ID, null, MainActivity.this);
        }
    }


    @Override
    public void onRefresh() {
        restartLoaderIfNetworkAvailable(false);
    }

    @Override
    public void onClick(Game game) {
        if (game.getRatio().compareTo(ZERO) == 0) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Details for this game are not available", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        Intent intent = new Intent(MainActivity.this, MatchDetailsActivity.class);
        intent.putExtra(EXTRA_MATCH_URL, game.getUrl());
        startActivity(intent);
    }

    @NonNull
    @Override
    public Loader<AsyncTaskListResult<Game>> onCreateLoader(int id, @Nullable Bundle args) {
        return new GamesAsyncTakLoader(MainActivity.this, this, winGuarantee, today, owned, sortOrderId);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<AsyncTaskListResult<Game>> loader, AsyncTaskListResult<Game> data) {
        swipeRefreshLayout.setRefreshing(false);
        matchesAdapter.setTouchEnable(true);
        long duration = System.currentTimeMillis() - executionTime;

        int count = 0;

        if (data.getError() != null) {
            matchesAdapter.setDataSet(null);
            createErrorAlertWithText(data.getError().getMessage(), null);
        }
        if (data.getResult() != null) {
            if (data.getResult().size() != 0) {
                matchesAdapter.setDataSet(data.getResult());
                tvError.setVisibility(GONE);
                rvMatches.setVisibility(VISIBLE);
                count = data.getResult().size();
                showCombinations();
            } else {
                matchesAdapter.setDataSet(null);
                createErrorAlertWithText("No matches found. Please try another filter", "No matches found. Please try another filter");
            }
        }


        int scanned = PrefUtils.getIntFromKey(MainActivity.this, PREF_SCANNED_COUNT, 0);
        int combinations = PrefUtils.getIntFromKey(MainActivity.this, PREF_COMBINATIONS_COUNT, 0);

        Bundle bundle = new Bundle();
        bundle.putInt("matches_found", count);
        bundle.putInt("matches_scanned", scanned);
        bundle.putInt("total_combinations", combinations);
        bundle.putLong("duration", duration);
        mFirebaseAnalytics.logEvent("load_matches", bundle);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<AsyncTaskListResult<Game>> loader) {
    }

    @Override
    public void beforeExecute() {
        matchesAdapter.setDataSet(null);
        matchesAdapter.setTouchEnable(false);
        swipeRefreshLayout.setRefreshing(true);
        tvError.setVisibility(View.INVISIBLE);
        executionTime = System.currentTimeMillis();
    }

    private void createErrorAlertWithText(String errorMessage, String tvErrorMessage) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("Oops...");
        sweetAlertDialog.setContentText(errorMessage);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.showCancelButton(false);
        sweetAlertDialog.setOnShowListener(dialogInterface -> swipeRefreshLayout.setRefreshing(false));
        sweetAlertDialog.setConfirmClickListener(sDialog -> {
            tvError.setVisibility(VISIBLE);
            if (tvErrorMessage != null) {
                tvError.setText(tvErrorMessage);
            }
            showCombinations();
            rvMatches.setVisibility(GONE);
            sDialog.dismissWithAnimation();
        });
        sweetAlertDialog.show();
    }

    private void setupWorkers() {
        WorkManager workManager = WorkManager.getInstance();

        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest
                .Builder(FindMatchesWorker.class, 40, MINUTES, 20, MINUTES)
                .setConstraints(new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.UNMETERED)
                        .build())
                .addTag(FIND_MATCHES_UPDATE_WORK)
                .build();

        workManager.enqueueUniquePeriodicWork(FIND_MATCHES_UPDATE_WORK, REPLACE, periodicWorkRequest);
    }

    private void showCombinations() {
        int combinations = PrefUtils.getIntFromKey(MainActivity.this, PREF_COMBINATIONS_COUNT, 0);

        Snackbar snackbar = Snackbar.make(drawer, format(Locale.getDefault(), "Analyzed %d combinations", combinations), Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}
