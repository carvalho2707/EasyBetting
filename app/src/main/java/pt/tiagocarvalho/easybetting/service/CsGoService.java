package pt.tiagocarvalho.easybetting.service;

import android.content.Context;
import android.preference.PreferenceManager;

import org.apache.commons.validator.routines.BigDecimalValidator;
import org.jsoup.Connection;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import pt.tiagocarvalho.easybetting.MainActivity;
import pt.tiagocarvalho.easybetting.data.EasyBettingDatabase;
import pt.tiagocarvalho.easybetting.data.Provider;
import pt.tiagocarvalho.easybetting.entity.BetProvider;
import pt.tiagocarvalho.easybetting.entity.Game;
import pt.tiagocarvalho.easybetting.entity.MatchDetailsVO;
import pt.tiagocarvalho.easybetting.entity.ProviderVO;
import pt.tiagocarvalho.easybetting.utils.Utils;
import timber.log.Timber;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;
import static org.jsoup.Jsoup.connect;
import static pt.tiagocarvalho.easybetting.utils.Utils.isToday;

public class CsGoService {
    private static final String BASE_URL = "https://www.hltv.org/matches/";
    private static final String BASE_MATCH_URL = "https://www.hltv.org";
    private static final String PROVIDER_URL = "https://www.hltv.org/bets/money";
    private static final int DEFAULT_SCALE = 10;
    private int counter;
    private int combinations;
    private Map<String, String> cookies;

    public List<Game> getMatches(boolean winGuarantee, boolean today, boolean owned, Context context, BigDecimal threshold) throws IOException, ParseException {
        Connection.Response matchesPages;
        try {
            matchesPages = getMatchesPages();
        } catch (SocketTimeoutException e) {
            return Collections.emptyList();
        }
        cookies = matchesPages.cookies();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        EasyBettingDatabase db = EasyBettingDatabase.getInstance(context);
        List<Provider> providers = db.providerDao().getAllActive();

        counter = 0;
        combinations = 0;
        Element body = matchesPages.parse().body();
        Elements upcomingMatches = body.getElementsByClass("upcoming-matches");
        Node matchesNode = upcomingMatches.first().childNode(1);
        List<Game> result = new ArrayList<>();
        for (int i = 1; i < matchesNode.childNodes().size(); i += 2) {
            Node node = matchesNode.childNode(i);
            String date = node.childNode(0).childNode(0).toString();
            if (today && !isToday(format.parse(date))) {
                break;
            }

            List<Game> games = parseDay(matchesNode.childNode(i), winGuarantee, owned, threshold, providers);
            result.addAll(games);
        }

        updateScannedGames(context);
        updateCombinations(context);
        return result;
    }

    private List<Game> parseDay(Node node, boolean winGuarantee, boolean owned, BigDecimal threshold, List<Provider> providers) {
        List<Game> games = new ArrayList<>();
        for (int i = 1; i < node.childNodes().size(); i++) {
            try {
                Game game = parseGame(node.childNode(i), owned, providers);
                if (game != null) {
                    counter++;
                    //if threshold is null comes from loader , else from notifications
                    if (threshold == null) {
                        boolean result = filterWinGuarantee(winGuarantee, game);
                        if (result) {
                            games.add(game);
                        }
                    } else if (game.getRatio().compareTo(threshold) >= 0) {
                        games.add(game);
                    }
                }
            } catch (SocketTimeoutException e) {
                Timber.e("SocketTimeoutException");
            } catch (IOException iow) {
                Timber.e("IOException");
            }
        }
        return games;
    }

    private boolean filterWinGuarantee(boolean winGuarantee, Game game) {
        return !winGuarantee || game.getRatio().compareTo(ONE) > 0;
    }

    private Game parseGame(Node node, boolean owned, List<Provider> providers) throws IOException {
        Set<String> ignored = new HashSet<>();

        String timestamp = ((Element) node).getElementsByClass("time").get(0).childNode(1).attr("data-unix");

        Elements teams = ((Element) node).getElementsByClass("line-align");
        if (teams.size() < 2) {
            return null;
        }
        String team1ImageUrl = teams.get(0).childNode(0).attr("src");
        String team2ImageUrl = teams.get(1).childNode(0).attr("src");

        List<Node> tableData = node.childNode(1).childNode(1).childNode(0).childNodes();
        String attrHref = node.attr("href");
        String matchId = attrHref.split("/")[2];
        String matchDetailsUrl = BASE_MATCH_URL + attrHref;
        if (tableData.size() < 6) {
            return null;
        }

        String team1 = tableData.get(3).childNode(1).childNode(2).childNode(0).toString().replaceAll("\n", "");
        if (tableData.get(7).childNode(1).childNodes().size() < 2) {
            return null;
        }
        String team2 = tableData.get(7).childNode(1).childNode(2).childNode(0).toString().replaceAll("\n", "");
        String tournament = tableData.get(9).childNode(1).childNode(0).toString();


        Set<BetProvider> betProviders = parseOdds(matchDetailsUrl);
        BigDecimal odd1 = ZERO;
        BigDecimal odd2 = ZERO;

        String provider1 = null;
        String provider2 = null;

        BigDecimal maxRatio = ZERO;
        for (BetProvider betProvider : betProviders) {
            if (owned && Utils.providerMissing(providers, betProvider.getProvider())) {
                ignored.add(betProvider.getProvider());
                //Timber.e("Ignoring %s", betProvider.getProvider());
                continue;
            }
            for (BetProvider betProvider2 : betProviders) {
                if (owned && Utils.providerMissing(providers, betProvider2.getProvider())) {
                    ignored.add(betProvider2.getProvider());
                    //Timber.e("Ignoring %s", betProvider2.getProvider());
                    continue;
                }
                //System.out.println("Performing calc on : " + betProvider.getOdd1() + "( " + betProvider.getProvider() + " )" + " vs " + betProvider2.getOdd2() + "( " + betProvider2.getProvider() + " )");

                BigDecimal temp1 = betProvider.getOdd1().divide(betProvider2.getOdd2(), DEFAULT_SCALE, HALF_UP);
                BigDecimal temp2 = ONE.add(temp1);
                BigDecimal ratio = betProvider.getOdd1().divide(temp2, DEFAULT_SCALE, HALF_UP);
                combinations++;
                if (ratio.compareTo(maxRatio) > 0) {
                    maxRatio = ratio;
                    odd1 = betProvider.getOdd1();
                    odd2 = betProvider2.getOdd2();
                    provider1 = betProvider.getProvider();
                    provider2 = betProvider2.getProvider();
                }
            }
        }

        Timber.e("Ignored: %s", ignored);
        return Game.builder()
                .id(matchId)
                .url(matchDetailsUrl)
                .tournament(tournament)
                .date(timestamp)
                .team1(team1)
                .team2(team2)
                .team1ImageUrl(team1ImageUrl)
                .team2ImageUrl(team2ImageUrl)
                .ratio(maxRatio)
                .odd1(odd1.setScale(2, HALF_UP).toPlainString())
                .odd2(odd2.setScale(2, HALF_UP).toPlainString())
                .provider1(provider1)
                .provider2(provider2)
                .build();
    }

    private Connection.Response getMatchesPages() throws IOException {
        return connect(BASE_URL)
                .method(Connection.Method.GET)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                .execute();
    }

    private Connection.Response getMatchDetailsPage(String url) throws IOException {
        Timber.e("Fetching page : %s", url);

        if (cookies == null) {
            return connect(url)
                    .method(Connection.Method.GET)
                    .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                    .execute();
        }

        return connect(url)
                .method(Connection.Method.GET)
                .cookies(cookies)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                .execute();
    }

    private Connection.Response getProvidersPage() throws IOException {
        return connect(PROVIDER_URL)
                .method(Connection.Method.GET)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
                .execute();
    }

    private Set<BetProvider> parseOdds(String url) throws IOException {
        Connection.Response response = getMatchDetailsPage(url);
        cookies = response.cookies();
        Element body = response.parse().body();
        return getOddsFromElement(body);
    }

    private Set<BetProvider> getOddsFromElement(Element body) {
        BigDecimalValidator bigDecimalValidator = BigDecimalValidator.getInstance();
        Set<BetProvider> result = new HashSet<>();
        Elements tables = body.getElementsByClass("betting");
        Node nodeParent = tables.get(0).childNode(1).childNode(1);
        for (int i = 2; i < nodeParent.childNodes().size(); i += 2) {
            Node row = nodeParent.childNode(i);
            if (row.childNodes().size() < 6) {
                continue;
            }
            String provider = "";

            String team1 = row.childNode(3).childNode(0).childNode(0).toString().replaceAll("\n", "");
            String team2 = row.childNode(7).childNode(0).childNode(0).toString().replaceAll("\n", "");

            try {
                String providerClass = row.childNode(1).childNode(0).childNode(0).attr("class").split(" ")[1];
                provider = providerClass.split("-")[1];
            } catch (Exception ignored) {
                Timber.e(team1 + " vs " + team2);
            }

            BigDecimal odd1 = bigDecimalValidator.validate(team1);
            BigDecimal odd2 = bigDecimalValidator.validate(team2);

            if (odd1 != null && odd2 != null) {
                result.add(BetProvider.builder()
                        .odd1(new BigDecimal(team1))
                        .odd2(new BigDecimal(team2))
                        .provider(provider)
                        .build());
            }
        }

        return result;
    }

    public MatchDetailsVO getMatchDetails(String url, boolean owned, Context context) throws IOException {
        EasyBettingDatabase db = EasyBettingDatabase.getInstance(context);
        List<Provider> providers = db.providerDao().getAllActive();
        Connection.Response response = getMatchDetailsPage(url);
        Element body = response.parse().body();

        Elements timeAndEvent = body.getElementsByClass("timeAndEvent");
        String unixTime = timeAndEvent.get(0).childNode(1).attr("data-unix");

        Elements logoElements = body.getElementsByClass("logo");
        Element team1 = logoElements.get(3);
        String image1 = team1.attr("src");
        String name1 = team1.attr("title");

        Element team2 = logoElements.get(4);
        String image2 = team2.attr("src");
        String name2 = team2.attr("title");

        Elements tournamentTitle = body.getElementsByClass("event text-ellipsis");
        String tournament = tournamentTitle.get(0).childNode(0).attr("title");

        Set<BetProvider> betProviders = getOddsFromElement(body);

        BigDecimal maxRatio = ZERO;
        String provider1 = null;
        String provider2 = null;
        BigDecimal oddMax1 = null;
        BigDecimal oddMax2 = null;
        for (BetProvider betProvider : new ArrayList<>(betProviders)) {
            if (owned && Utils.providerMissing(providers, betProvider.getProvider())) {
                continue;
            }

            for (BetProvider betProvider2 : new ArrayList<>(betProviders)) {
                if (owned && Utils.providerMissing(providers, betProvider2.getProvider())) {
                    continue;
                }

                BigDecimal temp1 = betProvider.getOdd1().divide(betProvider2.getOdd2(), DEFAULT_SCALE, HALF_UP);
                BigDecimal temp2 = ONE.add(temp1);
                BigDecimal ratio = betProvider.getOdd1().divide(temp2, DEFAULT_SCALE, HALF_UP);
                if (ratio.compareTo(maxRatio) > 0) {
                    maxRatio = ratio;
                    provider1 = betProvider.getProvider();
                    provider2 = betProvider2.getProvider();
                    oddMax1 = betProvider.getOdd1();
                    oddMax2 = betProvider2.getOdd2();
                }
            }
        }

        return MatchDetailsVO.builder()
                .time(unixTime)
                .team1(name2)
                .team2(name1)
                .tournament(tournament)
                .imageUrl1(image2)
                .imageUrl2(image1)
                .ratio(maxRatio)
                .odd1(oddMax1)
                .odd2(oddMax2)
                .provider1(provider1)
                .provider2(provider2)
                .build();
    }

    @Deprecated
    public List<ProviderVO> getProviders() throws IOException {
        List<ProviderVO> result = new ArrayList<>();
        Connection.Response providersResponse = getProvidersPage();
        Element body = providersResponse.parse().body();
        Elements bookmakers = body.getElementsByClass("provider-cell");
        for (Element element : bookmakers) {
            String title = element.childNode(0).childNode(0).attr("title");
            String img = BASE_MATCH_URL + element.childNode(0).childNode(0).attr("src");
            String url = element.childNode(0).attr("href");
            result.add(ProviderVO.builder()
                    .name(title)
                    .imageUrl(img)
                    .url(url)
                    .build());
        }
        return result;
    }

    private void updateScannedGames(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putInt(MainActivity.PREF_SCANNED_COUNT, counter).apply();
    }

    private void updateCombinations(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putInt(MainActivity.PREF_COMBINATIONS_COUNT, combinations).apply();
    }
}
