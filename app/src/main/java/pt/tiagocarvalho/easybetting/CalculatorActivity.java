package pt.tiagocarvalho.easybetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;

import static butterknife.ButterKnife.bind;
import static java.lang.String.format;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getStringFromKey;
import static pt.tiagocarvalho.easybetting.utils.Utils.isBlank;

public class CalculatorActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int DEFAULT_CALC_SCALE = 10;
    private static final String DEFAULT_SHOW_SCALE = "2";
    private static final String PREF_DECIMAL_PLACES_KEY = "pref_decimal_places_key";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.etAmount)
    EditText etAmount;
    @BindView(R.id.etOdd1)
    EditText etOdd1;
    @BindView(R.id.etOdd2)
    EditText etOdd2;
    @BindView(R.id.tvResult)
    TextView tvResult;
    @BindView(R.id.tvEarnings)
    TextView tvEarnings;
    @BindView(R.id.tvBet1)
    TextView tvBet1;
    @BindView(R.id.tvBet2)
    TextView tvBet2;
    @BindView(R.id.tvGains1)
    TextView tvGains1;
    @BindView(R.id.tvGains2)
    TextView tvGains2;
    @BindView(R.id.seekBar)
    SeekBar seekBar;

    private NumberFormat numberFormat;
    private int presentationScale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Calculator");
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        numberFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "PT"));
        numberFormat.setMinimumFractionDigits(presentationScale);
        numberFormat.setMaximumFractionDigits(presentationScale);

        navigationView.setNavigationItemSelectedListener(this);

        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateUI(etOdd1.getText().toString(), etOdd2.getText().toString(), s.toString(), seekBar.getProgress());
            }
        });

        etOdd1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateUI(s.toString(), etOdd2.getText().toString(), etAmount.getText().toString(), seekBar.getProgress());
            }
        });

        etOdd2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateUI(etOdd1.getText().toString(), s.toString(), etAmount.getText().toString(), seekBar.getProgress());
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateUI(etOdd1.getText().toString(), etOdd2.getText().toString(), etAmount.getText().toString(), progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        setDefaultValues();
    }

    @Override
    protected void onResume() {
        super.onResume();

        presentationScale = getPresentationScale(this);

        numberFormat.setMinimumFractionDigits(presentationScale);
        numberFormat.setMaximumFractionDigits(presentationScale);

        updateUI(etOdd1.getText().toString(), etOdd2.getText().toString(), etAmount.getText().toString(), seekBar.getProgress());
    }

    private void setDefaultValues() {
        tvBet1.setText(numberFormat.format(0));
        tvBet2.setText(numberFormat.format(0));
        tvGains1.setText(numberFormat.format(0));
        tvGains2.setText(numberFormat.format(0));
        tvEarnings.setText(numberFormat.format(0));
        tvResult.setText(new BigDecimal("0.00").setScale(3, HALF_UP).toString());
        tvResult.setTextColor(ContextCompat.getColor(this, R.color.black));
        tvEarnings.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    private void updateUI(String odd1, String odd2, String amount, int progress) {
        int val = 2 * progress - 100;


        BigDecimal oddA = isBlank(odd1) ? ZERO : new BigDecimal(odd1);
        BigDecimal oddB = isBlank(odd2) ? ZERO : new BigDecimal(odd2);
        BigDecimal total = isBlank(amount) ? ZERO : new BigDecimal(amount);

        if (oddA.compareTo(ZERO) == 0
                || oddB.compareTo(ZERO) == 0
                || total.compareTo(ZERO) == 0) {
            setDefaultValues();
            return;
        }

        BigDecimal temp1 = oddA.divide(oddB, DEFAULT_CALC_SCALE, RoundingMode.HALF_UP);
        BigDecimal temp2 = ONE.add(temp1);
        BigDecimal betA = total.divide(temp2, DEFAULT_CALC_SCALE, RoundingMode.HALF_UP);
        BigDecimal ratio = oddA.divide(temp2, DEFAULT_CALC_SCALE, HALF_UP);

        BigDecimal minA = total.divide(oddA, DEFAULT_CALC_SCALE, HALF_UP);
        BigDecimal minB = total.divide(oddB, DEFAULT_CALC_SCALE, HALF_UP);
        BigDecimal maxA = total.subtract(minB);

        BigDecimal toBetA;
        BigDecimal toBetB;
        BigDecimal hundred = new BigDecimal(100);
        if (val < 0) {
            BigDecimal inner = maxA.subtract(betA);
            BigDecimal i = new BigDecimal(Math.abs(val));
            BigDecimal inner2 = i.divide(hundred, DEFAULT_CALC_SCALE, HALF_UP);
            BigDecimal join = inner.multiply(inner2);
            toBetA = join.add(betA);
            toBetB = total.subtract(toBetA);
        } else {
            BigDecimal inner = minA.subtract(betA);
            BigDecimal i = new BigDecimal(Math.abs(val));
            BigDecimal inner2 = i.divide(hundred, DEFAULT_CALC_SCALE, HALF_UP);
            BigDecimal join = inner.multiply(inner2);
            toBetA = join.add(betA);
            toBetB = total.subtract(toBetA);
        }


        BigDecimal gainsA = toBetA.multiply(oddA).subtract(total);
        BigDecimal gainsB = toBetB.multiply(oddB).subtract(total);
        BigDecimal earnings = gainsA.max(gainsB);

        if (gainsA.compareTo(new BigDecimal("0.001")) < 0 && gainsA.compareTo(new BigDecimal("-0.001")) > 0) {
            gainsA = gainsA.abs();
        }

        if (gainsB.compareTo(new BigDecimal("0.001")) < 0 && gainsB.compareTo(new BigDecimal("-0.001")) > 0) {
            gainsB = gainsB.abs();
        }

        if (earnings.compareTo(new BigDecimal("0.001")) < 0 && earnings.compareTo(new BigDecimal("-0.001")) > 0) {
            earnings = earnings.abs();
        }

        tvBet1.setText(numberFormat.format(toBetA));
        tvBet2.setText(numberFormat.format(toBetB));
        tvGains1.setText(format(Locale.getDefault(), "( %s )", numberFormat.format(gainsA)));
        tvGains2.setText(format(Locale.getDefault(), "( %s )", numberFormat.format(gainsB)));
        tvEarnings.setText(numberFormat.format(earnings));
        tvResult.setText(ratio.setScale(3, HALF_UP).toString());


        if (ratio.compareTo(ONE) > 0) {
            tvResult.setTextColor(ContextCompat.getColor(this, R.color.material_green_500));
            tvEarnings.setTextColor(ContextCompat.getColor(this, R.color.material_green_500));
        } else {
            tvResult.setTextColor(ContextCompat.getColor(this, R.color.black));
            tvEarnings.setTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_matches) {
            Intent intent = new Intent(CalculatorActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_exchanges) {
            Intent intent = new Intent(CalculatorActivity.this, ProvidersActivity.class);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(CalculatorActivity.this, SettingsActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private int getPresentationScale(Context context) {
        String value = getStringFromKey(context, PREF_DECIMAL_PLACES_KEY, DEFAULT_SHOW_SCALE);
        return Integer.valueOf(value);
    }
}
