package pt.tiagocarvalho.easybetting.adapter;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import pt.tiagocarvalho.easybetting.R;
import pt.tiagocarvalho.easybetting.entity.Game;
import pt.tiagocarvalho.easybetting.service.SvgDecoder;
import pt.tiagocarvalho.easybetting.service.SvgDrawableTranscoder;
import pt.tiagocarvalho.easybetting.service.SvgSoftwareLayerSetter;

import static android.graphics.Typeface.DEFAULT;
import static android.graphics.Typeface.DEFAULT_BOLD;
import static butterknife.ButterKnife.bind;
import static java.lang.String.format;

public class MatchesAdapter extends RecyclerView.Adapter<MatchesAdapter.MyViewHolder> {

    private final MatchesAdapterOnClickHandler clickHandler;
    private final Context context;
    private final DateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    private final DateFormat formatTime = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private final GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private List<Game> dataSet;
    private boolean isTouchEnable = true;

    public MatchesAdapter(ArrayList<Game> data, Context context, MatchesAdapterOnClickHandler clickHandler) {
        this.dataSet = data;
        this.context = context;
        this.clickHandler = clickHandler;
        requestBuilder = Glide.with(context)
                .using(Glide.buildStreamModelLoader(Uri.class, context), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.not_found)
                .error(R.drawable.not_found)
                .listener(new SvgSoftwareLayerSetter<>());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int listPosition) {

        TextView tvTournament = holder.tvTournament;
        TextView tvDate = holder.tvDate;
        TextView tvTime = holder.tvTime;
        ImageView ivTeam1 = holder.ivTeam1;
        ImageView ivTeam2 = holder.ivTeam2;
        TextView tvRatio = holder.tvRatio;
        TextView tvOdd1 = holder.tvOdd1;
        TextView tvOdd2 = holder.tvOdd2;
        TextView tvProvider1 = holder.tvProvider1;
        TextView tvProvider2 = holder.tvProvider2;

        Game game = dataSet.get(listPosition);

        String tournamentTitle = fixTournamentTitle(game.getTournament());
        tvTournament.setText(tournamentTitle);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(game.getDate()));

        tvDate.setText(formatDate.format(calendar.getTime()));
        tvTime.setText(formatTime.format(calendar.getTime()));
        BigDecimal ratio = game.getRatio();
        tvOdd1.setText(game.getOdd1());
        tvOdd2.setText(game.getOdd2());
        tvProvider1.setText(game.getProvider1());
        tvProvider2.setText(game.getProvider2());

        if (ratio.compareTo(BigDecimal.ONE) > 0) {
            tvRatio.setTextColor(ContextCompat.getColor(context, R.color.material_green_500));
            tvRatio.setTypeface(DEFAULT_BOLD);
        } else {
            tvRatio.setTextColor(ContextCompat.getColor(context, R.color.material_grey_500));
            tvRatio.setTypeface(DEFAULT);
        }
        tvRatio.setText(format(Locale.getDefault(), "x%s", game.getRatio().setScale(3, RoundingMode.HALF_UP)));

        Glide
                .with(context)
                .load(game.getTeam1ImageUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Uri uri = Uri.parse(game.getTeam1ImageUrl());
                        requestBuilder
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .load(uri)
                                .into(ivTeam1);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(ivTeam1);

        Glide
                .with(context)
                .load(game.getTeam2ImageUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Uri uri = Uri.parse(game.getTeam2ImageUrl());
                        requestBuilder
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .load(uri)
                                .into(ivTeam2);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(ivTeam2);
    }

    private String fixTournamentTitle(String tournament) {
        return tournament.replace("&amp;", "&");
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public void setDataSet(List<Game> dataSet) {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    public void setTouchEnable(boolean enable) {
        this.isTouchEnable = enable;
    }

    public interface MatchesAdapterOnClickHandler {
        void onClick(Game game);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvTournament)
        TextView tvTournament;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.ivTeam1)
        ImageView ivTeam1;
        @BindView(R.id.ivTeam2)
        ImageView ivTeam2;
        @BindView(R.id.tvRatio)
        TextView tvRatio;
        @BindView(R.id.tvOdd1)
        TextView tvOdd1;
        @BindView(R.id.tvOdd2)
        TextView tvOdd2;
        @BindView(R.id.tvName1)
        TextView tvProvider1;
        @BindView(R.id.tvName2)
        TextView tvProvider2;

        MyViewHolder(View itemView) {
            super(itemView);
            bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (!isTouchEnable) {
                return;
            }

            if (clickHandler != null) {
                int adapterPosition = getAdapterPosition();
                Game game = dataSet.get(adapterPosition);
                clickHandler.onClick(game);
            }
        }
    }
}
