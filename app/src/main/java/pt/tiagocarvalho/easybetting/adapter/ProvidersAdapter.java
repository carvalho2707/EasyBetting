package pt.tiagocarvalho.easybetting.adapter;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import pt.tiagocarvalho.easybetting.R;
import pt.tiagocarvalho.easybetting.data.Provider;
import pt.tiagocarvalho.easybetting.service.SvgDecoder;
import pt.tiagocarvalho.easybetting.service.SvgDrawableTranscoder;
import pt.tiagocarvalho.easybetting.service.SvgSoftwareLayerSetter;

import static butterknife.ButterKnife.bind;

public class ProvidersAdapter extends RecyclerView.Adapter<ProvidersAdapter.MyViewHolder> {

    private final ProvidersAdapterOnClickHandler clickHandler;
    private final Context context;
    private final GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private List<Provider> dataSet;
    private boolean isTouchEnable = true;

    public ProvidersAdapter(ArrayList<Provider> data, Context context, ProvidersAdapterOnClickHandler clickHandler) {
        this.dataSet = data;
        this.context = context;
        this.clickHandler = clickHandler;
        requestBuilder = Glide.with(context)
                .using(Glide.buildStreamModelLoader(Uri.class, context), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.not_found)
                .error(R.drawable.not_found)
                .listener(new SvgSoftwareLayerSetter<>());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.provider_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int listPosition) {
        ImageView ivChecked = holder.ivChecked;
        ImageView ivLogo = holder.ivLogo;

        Provider provider = dataSet.get(listPosition);

        if (provider.getImageUrl() != null) {
            loadImage(provider.getImageUrl(), ivLogo);
        }

        if (provider.isState()) {
            ivChecked.setVisibility(View.VISIBLE);
        } else {
            ivChecked.setVisibility(View.INVISIBLE);
        }
    }

    private void loadImage(String imageUrl, ImageView iv) {
        Glide
                .with(context)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Uri uri = Uri.parse(imageUrl);
                        requestBuilder
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .load(uri)
                                .into(iv);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(iv);
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public void setDataSet(List<Provider> dataSet) {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    public void setTouchEnable(boolean enable) {
        this.isTouchEnable = enable;
    }

    public interface ProvidersAdapterOnClickHandler {
        void onClick(Provider provider);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivLogo)
        ImageView ivLogo;
        @BindView(R.id.ivChecked)
        ImageView ivChecked;

        MyViewHolder(View itemView) {
            super(itemView);
            bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (!isTouchEnable) {
                return;
            }

            if (clickHandler != null) {
                int adapterPosition = getAdapterPosition();
                Provider provider = dataSet.get(adapterPosition);
                clickHandler.onClick(provider);
            }
        }
    }
}
