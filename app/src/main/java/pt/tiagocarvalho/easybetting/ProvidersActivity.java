package pt.tiagocarvalho.easybetting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pt.tiagocarvalho.easybetting.adapter.ProvidersAdapter;
import pt.tiagocarvalho.easybetting.data.Provider;
import pt.tiagocarvalho.easybetting.network.AsyncTaskListResult;
import pt.tiagocarvalho.easybetting.network.AsyncTaskLoaderListener;
import pt.tiagocarvalho.easybetting.network.ProvidersAsyncTakLoader;

import static butterknife.ButterKnife.bind;
import static pt.tiagocarvalho.easybetting.utils.Utils.calculateNoOfColumns;
import static pt.tiagocarvalho.easybetting.utils.Utils.isNetworkUnavailable;

public class ProvidersActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SwipeRefreshLayout.OnRefreshListener,
        LoaderManager.LoaderCallbacks<AsyncTaskListResult<Provider>>,
        ProvidersAdapter.ProvidersAdapterOnClickHandler,
        AsyncTaskLoaderListener {

    public static final String EXTRA_PROVIDER_NAME = "pt.tiagocarvalho.easybetting.extra.provider.name";
    private static final int PROVIDERS_LOADER_ID = 102;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.rvMatches)
    RecyclerView rvMatches;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tvError)
    TextView tvError;

    private ProvidersAdapter providersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_providers);
        bind(this);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Exchanges");
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        providersAdapter = new ProvidersAdapter(null, ProvidersActivity.this, this);
        rvMatches.setAdapter(providersAdapter);
        rvMatches.setHasFixedSize(true);
        rvMatches.setLayoutManager(new GridLayoutManager(this, calculateNoOfColumns(getApplicationContext(), 250)));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.colorPrimaryLight),
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimaryText),
                getResources().getColor(R.color.colorSecondaryText));
        swipeRefreshLayout.setRefreshing(true);

        restartLoaderIfNetworkAvailable(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(ProvidersActivity.this, SettingsActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_matches) {
            Intent intent = new Intent(ProvidersActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_calculator) {
            Intent intent = new Intent(ProvidersActivity.this, CalculatorActivity.class);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void restartLoaderIfNetworkAvailable(boolean init) {
        if (isNetworkUnavailable(ProvidersActivity.this)) {
            createErrorAlertWithText("No network available");
            return;
        }

        if (init) {
            getSupportLoaderManager().initLoader(PROVIDERS_LOADER_ID, null, this);
        } else {
            getSupportLoaderManager().restartLoader(PROVIDERS_LOADER_ID, null, ProvidersActivity.this);
        }
    }


    @Override
    public void onRefresh() {
        restartLoaderIfNetworkAvailable(false);
    }

    @Override
    public void onClick(Provider provider) {
        Intent intent = new Intent(ProvidersActivity.this, ProviderDetailsActivity.class);
        intent.putExtra(EXTRA_PROVIDER_NAME, provider.getName());
        startActivity(intent);
    }

    @NonNull
    @Override
    public Loader<AsyncTaskListResult<Provider>> onCreateLoader(int id, @Nullable Bundle args) {
        return new ProvidersAsyncTakLoader(ProvidersActivity.this, this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<AsyncTaskListResult<Provider>> loader, AsyncTaskListResult<Provider> data) {
        swipeRefreshLayout.setRefreshing(false);
        providersAdapter.setTouchEnable(true);
        if (data.getError() != null) {
            createErrorAlertWithText(data.getError().getMessage());
        }
        if (data.getResult() != null) {
            providersAdapter.setDataSet(data.getResult());
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<AsyncTaskListResult<Provider>> loader) {
        providersAdapter.setDataSet(null);
    }

    @Override
    public void beforeExecute() {
        providersAdapter.setTouchEnable(false);
        swipeRefreshLayout.setRefreshing(true);
        tvError.setVisibility(View.INVISIBLE);
        rvMatches.setVisibility(View.VISIBLE);
    }

    private void createErrorAlertWithText(String errorMessage) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("Oops...");
        sweetAlertDialog.setContentText(errorMessage);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.showCancelButton(false);
        sweetAlertDialog.setOnShowListener(dialogInterface -> swipeRefreshLayout.setRefreshing(false));
        sweetAlertDialog.setConfirmClickListener(sDialog -> {
            tvError.setVisibility(View.VISIBLE);
            rvMatches.setVisibility(View.GONE);
            sDialog.dismissWithAnimation();
        });
        sweetAlertDialog.show();
    }
}
