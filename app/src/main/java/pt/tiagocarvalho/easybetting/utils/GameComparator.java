package pt.tiagocarvalho.easybetting.utils;

import java.util.Comparator;

import pt.tiagocarvalho.easybetting.MainActivity;
import pt.tiagocarvalho.easybetting.entity.Game;

public class GameComparator implements Comparator<Game> {
    private final int sortOrderId;

    public GameComparator(int sortOrderId) {
        this.sortOrderId = sortOrderId;
    }

    @Override
    public int compare(Game game, Game anotherGame) {
        if (sortOrderId == MainActivity.RATIO_ASC) {
            return game.getRatio().compareTo(anotherGame.getRatio());
        } else if (sortOrderId == MainActivity.RATIO_DESC) {
            return anotherGame.getRatio().compareTo(game.getRatio());
        } else if (sortOrderId == MainActivity.DATE_ASC) {
            Long date1 = Long.valueOf(game.getDate());
            Long date2 = Long.valueOf(anotherGame.getDate());
            return date1.compareTo(date2);
        } else if (sortOrderId == MainActivity.DATE_DESC) {
            Long date1 = Long.valueOf(game.getDate());
            Long date2 = Long.valueOf(anotherGame.getDate());
            return date2.compareTo(date1);
        }
        return 0;
    }
}
