package pt.tiagocarvalho.easybetting.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import pt.tiagocarvalho.easybetting.MainActivity;
import pt.tiagocarvalho.easybetting.MatchDetailsActivity;
import pt.tiagocarvalho.easybetting.R;

import static android.app.Notification.VISIBILITY_PRIVATE;
import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.NOTIFICATION_SERVICE;
import static android.graphics.Color.parseColor;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.O;
import static android.support.v4.app.NotificationCompat.PRIORITY_MAX;
import static java.lang.Boolean.TRUE;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getBooleanFromKey;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getStringFromKey;

public class NotificationUtils {
    private static final String PENDING_INVEST_CHANNEL_ID = "FIND_MATCHES_CHANNEL_ID";
    private static final String PENDING_INVEST_CHANNEL_NAME = "Find Matches Channel";
    private static final String NOTIFICATIONS_VIBRATE = "pref_notifications_vibrate";
    private static final String NOTIFICATIONS_RINGTONE = "pref_notifications_ringtone";
    private static final String NOTIFICATIONS_DEFAULT_RINGTONE = "content://settings/system/notification_sound";

    private static final String COLOR_MATERIAL_GREEN_500 = "#009688";

    public static void createNotification(Context context, int id, String contentTitle, String contentText, String url) {
        final NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }

        Uri notificationSoundUri = Uri.parse(getRingtone(context));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel groupChannel = new NotificationChannel(PENDING_INVEST_CHANNEL_ID, PENDING_INVEST_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            groupChannel.enableLights(true);

            if (isVibrationEnabled(context)) {
                groupChannel.enableVibration(true);
            }
            groupChannel.setLightColor(parseColor(COLOR_MATERIAL_GREEN_500));
            groupChannel.canShowBadge();
            groupChannel.setShowBadge(true);
            groupChannel.setLockscreenVisibility(VISIBILITY_PRIVATE);
            notificationManager.createNotificationChannel(groupChannel);
        }

        Intent intent;
        if (url != null) {
            intent = new Intent(context, MatchDetailsActivity.class);
            intent.putExtra(MainActivity.EXTRA_MATCH_URL, url);
        } else {
            intent = new Intent(context, MainActivity.class);
        }

        NotificationCompat.Builder summaryNotificationBuilder = new NotificationCompat.Builder(context, PENDING_INVEST_CHANNEL_ID)
                .setContentTitle(contentTitle)
                .setContentText(contentText + " Available")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(parseColor(COLOR_MATERIAL_GREEN_500))
                .setVisibility(VISIBILITY_PRIVATE)
                .setPriority(PRIORITY_MAX)
                .setAutoCancel(TRUE)
                .setSound(notificationSoundUri)
                .setContentIntent(PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        FLAG_UPDATE_CURRENT));

        if (SDK_INT < O) {
            summaryNotificationBuilder
                    .setColor(parseColor(COLOR_MATERIAL_GREEN_500))
                    .setVibrate(new long[]{100, 250})
                    .setLights(parseColor(COLOR_MATERIAL_GREEN_500), 500, 5000)
                    .setAutoCancel(true);
        }

        notificationManager.notify(id, summaryNotificationBuilder.build());
    }

    private static boolean isVibrationEnabled(Context context) {
        return getBooleanFromKey(context, NOTIFICATIONS_VIBRATE, TRUE);
    }

    private static String getRingtone(Context context) {
        return getStringFromKey(context, NOTIFICATIONS_RINGTONE, NOTIFICATIONS_DEFAULT_RINGTONE);
    }
}
