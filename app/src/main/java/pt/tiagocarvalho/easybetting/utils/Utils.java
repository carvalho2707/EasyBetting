package pt.tiagocarvalho.easybetting.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pt.tiagocarvalho.easybetting.data.Provider;

public class Utils {

    private Utils() {
    }

    public static boolean isNetworkUnavailable(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm == null) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork == null || !activeNetwork.isConnectedOrConnecting();
    }

    public static void setMenuItemActiveString(MenuItem menuItem, String prefix, String suffix) {
        menuItem.setTitle(prefix + " " + suffix);
    }

    public static boolean isToday(Date date) {
        Calendar today = Calendar.getInstance();
        Calendar specifiedDate = Calendar.getInstance();
        specifiedDate.setTime(date);

        return today.get(Calendar.DAY_OF_MONTH) == specifiedDate.get(Calendar.DAY_OF_MONTH)
                && today.get(Calendar.MONTH) == specifiedDate.get(Calendar.MONTH)
                && today.get(Calendar.YEAR) == specifiedDate.get(Calendar.YEAR);
    }

    public static boolean providerMissing(List<Provider> providerList, String provider) {
        List<String> result = new ArrayList<>();
        for (Provider provider1 : providerList) {
            result.add(provider1.getName());
            if (provider1.getSecondaryName() != null) {
                result.add(provider1.getSecondaryName());
            }
        }
        return !containsIgnoreCase(result, provider);
    }

    private static boolean containsIgnoreCase(List<String> aList, String toFind) {
        boolean contains = false;
        for (String string : aList) {
            if (string.equalsIgnoreCase(toFind)) {
                contains = true;
            }
        }
        return contains;
    }

    public static boolean isBlank(String string) {
        if (string == null || string.length() == 0)
            return true;

        int l = string.length();
        for (int i = 0; i < l; i++) {
            if (!isWhitespace(string.codePointAt(i)))
                return false;
        }
        return true;
    }

    private static boolean isWhitespace(int c) {
        return c == ' ' || c == '\t' || c == '\n' || c == '\f' || c == '\r';
    }

    public static int calculateNoOfColumns(@NonNull Context context, int minWidth) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / minWidth);
    }
}
