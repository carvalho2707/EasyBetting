package pt.tiagocarvalho.easybetting.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static java.lang.Boolean.FALSE;

public class PrefUtils {

    public static boolean getBooleanFromKey(Context context, String key) {
        return getBooleanFromKey(context, key, FALSE);
    }

    public static boolean getBooleanFromKey(Context context, String key, Boolean defaultValue) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, defaultValue);
    }

    public static int getIntFromKey(Context context, String key, int defaultValue) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return prefs.getInt(key, defaultValue);
    }

    public static String getStringFromKey(Context context, String key, String defaultValue) {
        SharedPreferences prefs = getDefaultSharedPreferences(context);
        return prefs.getString(key, defaultValue);
    }
}
