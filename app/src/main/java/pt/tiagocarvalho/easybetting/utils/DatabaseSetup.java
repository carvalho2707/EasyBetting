package pt.tiagocarvalho.easybetting.utils;

import java.util.List;

import pt.tiagocarvalho.easybetting.data.Provider;

import static java.util.Arrays.asList;

public class DatabaseSetup {

    public static List<Provider> setupDatabase() {
        Provider ggbet = Provider.builder()
                .name("ggbet")
                .imageUrl("https://www.hltv.org/img/static/betting/ggbetnew.png")
                .secondaryName(null)
                .state(false)
                .build();

        Provider pinnacle = Provider.builder()
                .name("pinnacle")
                .imageUrl("https://www.hltv.org/img/static/betting/pinnacle-esports-logo.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider _1xbet = Provider.builder()
                .name("1xbet")
                .imageUrl("https://www.hltv.org/img/static/betting/1xbet-light.svg")
                .secondaryName("1xstavka")
                .state(false)
                .build();

        Provider buff88 = Provider.builder()
                .name("buff88")
                .imageUrl("https://www.hltv.org/img/static/betting/buff8-day.png")
                .secondaryName(null)
                .state(false)
                .build();

        Provider betway = Provider.builder()
                .name("betway")
                .imageUrl("https://www.hltv.org/img/static/betting/betway.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider thunderpick = Provider.builder()
                .name("thunderpick")
                .imageUrl("https://www.hltv.org/img/static/betting/thunderpick.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider lootbet = Provider.builder()
                .name("lootbet")
                .imageUrl("https://www.hltv.org/img/static/betting/lootbet.png")
                .secondaryName(null)
                .state(false)
                .build();

        Provider bet365 = Provider.builder()
                .name("bet365")
                .imageUrl("https://www.hltv.org/img/static/betting/bet365.png")
                .secondaryName(null)
                .state(false)
                .build();

        Provider unibet = Provider.builder()
                .name("unibet")
                .imageUrl("https://www.hltv.org/img/static/betting/unibet.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider bravobet = Provider.builder()
                .name("bravobet")
                .imageUrl("https://www.hltv.org/img/static/betting/bravobet.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider makemybet = Provider.builder()
                .name("makemybet")
                .imageUrl("https://www.hltv.org/img/static/betting/makemybet.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider betit = Provider.builder()
                .name("betit")
                .imageUrl("https://www.hltv.org/img/static/betting/betit_day.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider _22bet = Provider.builder()
                .name("22bet")
                .imageUrl("https://www.hltv.org/img/static/betting/22bet-day.svg")
                .secondaryName(null)
                .state(false)
                .build();

        Provider egb = Provider.builder()
                .name("egb")
                .imageUrl("https://www.hltv.org/img/static/betting/egb.png")
                .secondaryName("egbnolink")
                .state(false)
                .build();


        return asList(ggbet, pinnacle, _1xbet, buff88, egb, _22bet, betit, makemybet, bet365);
    }
}
