package pt.tiagocarvalho.easybetting;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pt.tiagocarvalho.easybetting.entity.MatchDetailsVO;
import pt.tiagocarvalho.easybetting.network.AsyncTaskLoaderListener;
import pt.tiagocarvalho.easybetting.network.AsyncTaskResult;
import pt.tiagocarvalho.easybetting.network.MatchDetailsAsyncTakLoader;
import pt.tiagocarvalho.easybetting.service.SvgDecoder;
import pt.tiagocarvalho.easybetting.service.SvgDrawableTranscoder;
import pt.tiagocarvalho.easybetting.service.SvgSoftwareLayerSetter;

import static android.content.Intent.ACTION_VIEW;
import static android.graphics.Typeface.DEFAULT_BOLD;
import static butterknife.ButterKnife.bind;
import static cn.pedant.SweetAlert.SweetAlertDialog.ERROR_TYPE;
import static java.lang.Boolean.FALSE;
import static java.lang.String.format;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getBooleanFromKey;
import static pt.tiagocarvalho.easybetting.utils.PrefUtils.getStringFromKey;
import static pt.tiagocarvalho.easybetting.utils.Utils.isBlank;
import static pt.tiagocarvalho.easybetting.utils.Utils.isNetworkUnavailable;

public class MatchDetailsActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<AsyncTaskResult<MatchDetailsVO>>,
        AsyncTaskLoaderListener {

    private static final int MATCH_DETAILS_LOADER_ID = 101;
    private static final int DEFAULT_CALC_SCALE = 10;
    private static final String DEFAULT_SHOW_SCALE = "2";
    private static final int RATIO_SCALE = 3;
    private static final String PREF_DECIMAL_PLACES_KEY = "pref_decimal_places_key";
    private static final String KEY_FILTER_OWNED = "pt.tiagocarvalho.easybetting.filter.owned";
    private static final String STATE_AMOUNT = "STATE_AMOUNT";
    private static final String STATE_SEEK = "STATE_SEEK";
    private final DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
    @BindView(R.id.tvTournamentTitle)
    TextView tvTournamentTitle;
    @BindView(R.id.tvTeam1)
    TextView tvTeam1;
    @BindView(R.id.tvTeam2)
    TextView tvTeam2;
    @BindView(R.id.tvOdd1)
    TextView tvOdd1;
    @BindView(R.id.tvOdd2)
    TextView tvOdd2;
    @BindView(R.id.tvRatio)
    TextView tvRatio;
    @BindView(R.id.etValueBet)
    EditText etValueBet;
    @BindView(R.id.tvResult)
    TextView tvResult;
    @BindView(R.id.ivTeam1)
    ImageView ivTeam1;
    @BindView(R.id.ivTeam2)
    ImageView ivTeam2;
    @BindView(R.id.tvProvider1)
    TextView tvProvider1;
    @BindView(R.id.tvProvider2)
    TextView tvProvider2;
    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvProviderBet1)
    TextView tvProviderBet1;
    @BindView(R.id.tvProviderBet2)
    TextView tvProviderBet2;
    @BindView(R.id.tvAmount1)
    TextView tvAmount1;
    @BindView(R.id.tvAmount2)
    TextView tvAmount2;

    @BindView(R.id.tvGainsA)
    TextView tvGainsA;
    @BindView(R.id.tvGainsB)
    TextView tvGainsB;
    @BindView(R.id.seekBar2)
    SeekBar seekBar;

    private String matchUrl;
    private int presentationScale;
    private NumberFormat numberFormat;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_details);
        bind(this);

        ActionBar toolbar = getSupportActionBar();
        if (toolbar != null) {
            toolbar.setDisplayHomeAsUpEnabled(true);
            toolbar.setDisplayShowHomeEnabled(true);
        }

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        numberFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "PT"));
        numberFormat.setMinimumFractionDigits(presentationScale);
        numberFormat.setMaximumFractionDigits(presentationScale);

        matchUrl = extras.getString(MainActivity.EXTRA_MATCH_URL);

        requestBuilder = Glide.with(this)
                .using(Glide.buildStreamModelLoader(Uri.class, this), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.not_found)
                .error(R.drawable.not_found)
                .listener(new SvgSoftwareLayerSetter<>());

        etValueBet.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateUI(tvOdd1.getText().toString(), tvOdd2.getText().toString(), s.toString(), seekBar.getProgress());
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateUI(tvOdd1.getText().toString(), tvOdd2.getText().toString(), etValueBet.getText().toString(), progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        restartLoaderIfNetworkAvailable(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        presentationScale = getPresentationScale(this);

        numberFormat.setMinimumFractionDigits(presentationScale);
        numberFormat.setMaximumFractionDigits(presentationScale);

        updateUI(tvOdd1.getText().toString(), tvOdd2.getText().toString(), etValueBet.getText().toString(), seekBar.getProgress());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_AMOUNT, etValueBet.getText().toString());
        outState.putInt(STATE_SEEK, seekBar.getProgress());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_AMOUNT) && savedInstanceState.containsKey(STATE_SEEK)) {
            String amount = savedInstanceState.getString(STATE_AMOUNT);
            int progress = savedInstanceState.getInt(STATE_SEEK);
            updateUI(tvOdd1.getText().toString(), tvOdd2.getText().toString(), amount, progress);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_match_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(MatchDetailsActivity.this, SettingsActivity.class));
                return true;
            case R.id.action_refresh:
                restartLoaderIfNetworkAvailable(false);
                break;
            case R.id.action_open_website:
                Intent i = new Intent(ACTION_VIEW);
                i.setData(Uri.parse(matchUrl));
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateUI(String odd1, String odd2, String amount, int progress) {
        int val = 2 * progress - 100;

        BigDecimal oddA = isBlank(odd1) ? ZERO : new BigDecimal(odd1);
        BigDecimal oddB = isBlank(odd2) ? ZERO : new BigDecimal(odd2);
        BigDecimal total = isBlank(amount) ? ZERO : new BigDecimal(amount);

        if (oddA.compareTo(ZERO) == 0
                || oddB.compareTo(ZERO) == 0
                || total.compareTo(ZERO) == 0) {
            tvAmount1.setText(numberFormat.format(0));
            tvAmount2.setText(numberFormat.format(0));
            tvGainsA.setText(numberFormat.format(0));
            tvGainsB.setText(numberFormat.format(0));
            tvResult.setText(new BigDecimal("0.00").setScale(presentationScale, HALF_UP).toString());
            return;
        }

        BigDecimal temp1 = oddA.divide(oddB, DEFAULT_CALC_SCALE, RoundingMode.HALF_UP);
        BigDecimal temp2 = ONE.add(temp1);
        BigDecimal betA = total.divide(temp2, DEFAULT_CALC_SCALE, RoundingMode.HALF_UP);
        BigDecimal ratio = oddA.divide(temp2, DEFAULT_CALC_SCALE, HALF_UP);

        BigDecimal minA = total.divide(oddA, DEFAULT_CALC_SCALE, HALF_UP);
        BigDecimal minB = total.divide(oddB, DEFAULT_CALC_SCALE, HALF_UP);
        BigDecimal maxA = total.subtract(minB);

        BigDecimal toBetA;
        BigDecimal toBetB;
        BigDecimal hundred = new BigDecimal(100);
        if (val < 0) {
            BigDecimal inner = maxA.subtract(betA);
            BigDecimal i = new BigDecimal(Math.abs(val));
            BigDecimal inner2 = i.divide(hundred, DEFAULT_CALC_SCALE, HALF_UP);
            BigDecimal join = inner.multiply(inner2);
            toBetA = join.add(betA);
            toBetB = total.subtract(toBetA);
        } else {
            BigDecimal inner = minA.subtract(betA);
            BigDecimal i = new BigDecimal(Math.abs(val));
            BigDecimal inner2 = i.divide(hundred, DEFAULT_CALC_SCALE, HALF_UP);
            BigDecimal join = inner.multiply(inner2);
            toBetA = join.add(betA);
            toBetB = total.subtract(toBetA);
        }


        BigDecimal gainsA = toBetA.multiply(oddA).subtract(total);
        BigDecimal gainsB = toBetB.multiply(oddB).subtract(total);
        BigDecimal earnings = gainsA.max(gainsB);

        if (gainsA.compareTo(new BigDecimal("0.001")) < 0 && gainsA.compareTo(new BigDecimal("-0.001")) > 0) {
            gainsA = gainsA.abs();
        }

        if (gainsB.compareTo(new BigDecimal("0.001")) < 0 && gainsB.compareTo(new BigDecimal("-0.001")) > 0) {
            gainsB = gainsB.abs();
        }

        if (earnings.compareTo(new BigDecimal("0.001")) < 0 && earnings.compareTo(new BigDecimal("-0.001")) > 0) {
            earnings = earnings.abs();
        }

        tvAmount1.setText(numberFormat.format(toBetA));
        tvAmount2.setText(numberFormat.format(toBetB));
        tvGainsA.setText(format(Locale.getDefault(), "( %s )", numberFormat.format(gainsA)));
        tvGainsB.setText(format(Locale.getDefault(), "( %s )", numberFormat.format(gainsB)));
        tvResult.setText(numberFormat.format(earnings));

        if (ratio.compareTo(ONE) > 0) {
            tvResult.setTextColor(ContextCompat.getColor(this, R.color.material_green_500));
        } else {
            tvResult.setTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    @NonNull
    @Override
    public Loader<AsyncTaskResult<MatchDetailsVO>> onCreateLoader(int id, @Nullable Bundle args) {
        boolean owned = getBooleanFromKey(MatchDetailsActivity.this, KEY_FILTER_OWNED, FALSE);
        return new MatchDetailsAsyncTakLoader(MatchDetailsActivity.this, this, matchUrl, owned);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<AsyncTaskResult<MatchDetailsVO>> loader, AsyncTaskResult<MatchDetailsVO> data) {
        if (sweetAlertDialog != null && sweetAlertDialog.isShowing()) {
            sweetAlertDialog.dismissWithAnimation();
        }
        if (data.getError() != null) {
            createErrorAlertWithText(data.getError().getMessage());
        }
        if (data.getResult() != null) {

            MatchDetailsVO matchDetailsVO = data.getResult();

            loadImageView(matchDetailsVO.getImageUrl1(), ivTeam1);
            loadImageView(matchDetailsVO.getImageUrl2(), ivTeam2);

            tvProvider1.setText(matchDetailsVO.getProvider1());
            tvProviderBet1.setText(matchDetailsVO.getProvider1());
            tvProvider2.setText(matchDetailsVO.getProvider2());
            tvProviderBet2.setText(matchDetailsVO.getProvider2());

            tvTournamentTitle.setText(matchDetailsVO.getTournament());
            tvTeam1.setText(matchDetailsVO.getTeam1());
            tvTeam2.setText(matchDetailsVO.getTeam2());
            tvOdd1.setText(matchDetailsVO.getOdd1().toString());
            tvOdd2.setText(matchDetailsVO.getOdd2().toString());
            tvRatio.setText(matchDetailsVO.getRatio().setScale(RATIO_SCALE, HALF_UP).toString());
            if (matchDetailsVO.getRatio().compareTo(ONE) > 0) {
                tvRatio.setTextColor(ContextCompat.getColor(this, R.color.material_green_500));
                tvRatio.setTypeface(DEFAULT_BOLD);
            }
            Calendar specifiedDate = Calendar.getInstance();
            specifiedDate.setTimeInMillis(Long.valueOf(matchDetailsVO.getTime()));
            tvDate.setText(format.format(specifiedDate.getTime()));

            updateUI(tvOdd1.getText().toString(), tvOdd2.getText().toString(), "10", seekBar.getProgress());
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<AsyncTaskResult<MatchDetailsVO>> loader) {
    }

    @Override
    public void beforeExecute() {
        createLoginDialog();
    }

    private void createLoginDialog() {
        String colorAccent = "#" + Integer.toHexString(ContextCompat.getColor(this, R.color.colorAccent));
        sweetAlertDialog = new SweetAlertDialog(MatchDetailsActivity.this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading...");
        sweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor(colorAccent));
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
    }

    private void loadImageView(String url, ImageView imageView) {
        Glide
                .with(this)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Uri uri = Uri.parse(url);
                        requestBuilder
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .load(uri)
                                .into(imageView);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

    private void createErrorAlertWithText(String errorMessage) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, ERROR_TYPE);
        sweetAlertDialog.setTitleText("Oops...");
        sweetAlertDialog.setContentText(errorMessage);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.showCancelButton(false);
        sweetAlertDialog.setConfirmClickListener(SweetAlertDialog::dismissWithAnimation);
        sweetAlertDialog.show();
    }

    private void restartLoaderIfNetworkAvailable(boolean init) {
        if (isNetworkUnavailable(MatchDetailsActivity.this)) {
            createErrorAlertWithText("No network available");
            return;
        }

        if (init) {
            getSupportLoaderManager().initLoader(MATCH_DETAILS_LOADER_ID, null, this);
        } else {
            getSupportLoaderManager().restartLoader(MATCH_DETAILS_LOADER_ID, null, MatchDetailsActivity.this);
        }
    }

    private int getPresentationScale(Context context) {
        String value = getStringFromKey(context, PREF_DECIMAL_PLACES_KEY, DEFAULT_SHOW_SCALE);
        return Integer.valueOf(value);
    }

}
