package pt.tiagocarvalho.easybetting;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;
import java.lang.ref.WeakReference;

import butterknife.BindView;
import pt.tiagocarvalho.easybetting.data.EasyBettingDatabase;
import pt.tiagocarvalho.easybetting.data.Provider;
import pt.tiagocarvalho.easybetting.data.ProviderDao;
import pt.tiagocarvalho.easybetting.service.SvgDecoder;
import pt.tiagocarvalho.easybetting.service.SvgDrawableTranscoder;
import pt.tiagocarvalho.easybetting.service.SvgSoftwareLayerSetter;

import static butterknife.ButterKnife.bind;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static pt.tiagocarvalho.easybetting.ProvidersActivity.EXTRA_PROVIDER_NAME;

public class ProviderDetailsActivity extends AppCompatActivity {


    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.switchSate)
    Switch switchSate;
    @BindView(R.id.tvSate)
    TextView tvState;
    @BindView(R.id.btnSave)
    Button btnSave;

    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details);
        bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        requestBuilder = Glide.with(this)
                .using(Glide.buildStreamModelLoader(Uri.class, this), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.not_found)
                .error(R.drawable.not_found)
                .listener(new SvgSoftwareLayerSetter<>());

        loadEditInfo(extras);

        switchSate.setOnCheckedChangeListener((compoundButton, b) -> tvState.setText(b ? "Enabled" : "Disabled"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(ProviderDetailsActivity.this, SettingsActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadEditInfo(Bundle extras) {
        String provider = extras.getString(EXTRA_PROVIDER_NAME);
        this.name = provider;
        new DbLoadAsyncTask(new WeakReference<>(this), ProviderDetailsActivity.this).execute(provider);
    }

    private void loadImageView(String url) {
        Glide
                .with(this)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Uri uri = Uri.parse(url);
                        requestBuilder
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .load(uri)
                                .into(ivLogo);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(ivLogo);
    }

    public void save(View view) {
        boolean state = switchSate.isChecked();

        AsyncTaskInput asyncTaskInput = new AsyncTaskInput();
        asyncTaskInput.name = name;
        asyncTaskInput.state = state;

        new AsyncTaskRunner(view.getContext(), this).execute(asyncTaskInput);
    }

    private static class AsyncTaskRunner extends AsyncTask<AsyncTaskInput, Void, Void> {
        private final WeakReference<Context> mContext;
        private final WeakReference<ProviderDetailsActivity> mActivityRef;

        AsyncTaskRunner(Context context, ProviderDetailsActivity activity) {
            this.mContext = new WeakReference<>(context);
            this.mActivityRef = new WeakReference<>(activity);
        }

        @Override
        protected Void doInBackground(AsyncTaskInput... params) {
            AsyncTaskInput asyncTaskInput = params[0];

            if (mContext.get() == null || mActivityRef.get() == null) {
                throw new RuntimeException();
            }
            EasyBettingDatabase db = EasyBettingDatabase.getInstance(mContext.get());
            ProviderDao providerDao = db.providerDao();

            Provider provider = db.providerDao().getProviderByName(asyncTaskInput.name);
            provider.setState(asyncTaskInput.state);
            providerDao.update(provider);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Intent intent = new Intent();
            if (mActivityRef.get() != null) {
                mActivityRef.get().setResult(RESULT_OK, intent);
                mActivityRef.get().finish();
            }
        }
    }

    private static class DbLoadAsyncTask extends AsyncTask<String, Void, Provider> {
        private final WeakReference<Context> mContext;
        private final WeakReference<ProviderDetailsActivity> mActivityRef;

        DbLoadAsyncTask(WeakReference<Context> contextWeakReference, ProviderDetailsActivity activity) {
            mContext = contextWeakReference;
            mActivityRef = new WeakReference<>(activity);
        }

        protected Provider doInBackground(String... strings) {
            if (mContext.get() == null) {
                throw new RuntimeException();
            }

            EasyBettingDatabase db = EasyBettingDatabase.getInstance(mContext.get());
            return db.providerDao().getProviderByName(strings[0]);
        }

        @Override
        protected void onPostExecute(Provider provider) {
            super.onPostExecute(provider);
            mActivityRef.get().switchSate.setEnabled(TRUE);
            mActivityRef.get().btnSave.setEnabled(TRUE);


            mActivityRef.get().loadImageView(provider.getImageUrl());
            mActivityRef.get().switchSate.setChecked(provider.isState());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mActivityRef.get().switchSate.setEnabled(FALSE);
            mActivityRef.get().btnSave.setEnabled(FALSE);
        }
    }

    class AsyncTaskInput {
        String name;
        boolean state;
    }
}
