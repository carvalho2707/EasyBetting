package pt.tiagocarvalho.easybetting.workers;

import android.content.Context;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.List;

import androidx.work.Worker;
import pt.tiagocarvalho.easybetting.entity.Game;
import pt.tiagocarvalho.easybetting.service.CsGoService;
import pt.tiagocarvalho.easybetting.utils.NotificationUtils;
import pt.tiagocarvalho.easybetting.utils.PrefUtils;
import timber.log.Timber;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static pt.tiagocarvalho.easybetting.utils.Utils.isNetworkUnavailable;

public class FindMatchesWorker extends Worker {
    private static final Integer NOTIFICATION_BASE_ID = 1000;
    private static final String DEFAULT_THRESHOLD = "1.00";
    private static final String NOTIFICATIONS_ENABLE = "pref_notifications_enable_key";
    private static final String NOTIFICATIONS_THRESHOLD = "pref_notifications_threshold";
    private final CsGoService csGoService = new CsGoService();

    @NonNull
    @Override
    public Result doWork() {
        if (isNetworkUnavailable(getApplicationContext())) {
            return Result.RETRY;
        }

        if (!isNotificationEnabled(getApplicationContext())) {
            return Result.SUCCESS;
        }

        BigDecimal threshold = getThreshold(getApplicationContext());

        try {
            List<Game> matches = csGoService.getMatches(TRUE, FALSE, TRUE, getApplicationContext(), threshold);
            if (!matches.isEmpty()) {
                int i = NOTIFICATION_BASE_ID + 1;
                for (Game game : matches) {
                    NotificationUtils.createNotification(getApplicationContext(),
                            i,
                            game.getTeam1() + " vs " + game.getTeam2(),
                            "Ratio: " + game.getRatio().setScale(3, RoundingMode.HALF_UP),
                            game.getUrl());
                    i++;
                }
            }

        } catch (ParseException | IOException e) {
            Crashlytics.logException(e);
            Timber.e(e);
            return Result.RETRY;
        }

        return Result.SUCCESS;
    }


    private boolean isNotificationEnabled(Context context) {
        return PrefUtils.getBooleanFromKey(context, NOTIFICATIONS_ENABLE, TRUE);
    }

    private BigDecimal getThreshold(Context context) {
        String threshold = PrefUtils.getStringFromKey(context, NOTIFICATIONS_THRESHOLD, DEFAULT_THRESHOLD);
        return new BigDecimal(threshold);
    }
}
