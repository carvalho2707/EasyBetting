package pt.tiagocarvalho.easybetting.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.Executors;

import pt.tiagocarvalho.easybetting.utils.DatabaseSetup;

@Database(entities = {Provider.class}, version = 2)
public abstract class EasyBettingDatabase extends RoomDatabase {

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("INSERT INTO provider (name, secondary_name, image_url, state) VALUES('makemybet',NULL, 'https://www.hltv.org/img/static/betting/makemybet.svg', 0 )");
            database.execSQL("INSERT INTO provider (name, secondary_name, image_url, state) VALUES('betit',NULL, 'https://www.hltv.org/img/static/betting/betit_day.svg', 0 )");
            database.execSQL("INSERT INTO provider (name, secondary_name, image_url, state) VALUES('22bet',NULL, 'https://www.hltv.org/img/static/betting/22bet-day.svg', 0 )");
            database.execSQL("INSERT INTO provider (name, secondary_name, image_url, state) VALUES('egb','egbnolink', 'https://www.hltv.org/img/static/betting/egb.png', 0 )");
            database.execSQL("INSERT INTO provider (name, secondary_name, image_url, state) VALUES('bet365',NULL, 'https://www.hltv.org/img/static/betting/bet365.png', 0 )");
        }
    };
    private static volatile EasyBettingDatabase INSTANCE;

    public static EasyBettingDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (EasyBettingDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room
                            .databaseBuilder(context.getApplicationContext(), EasyBettingDatabase.class, "EasyBettingDatabase.db")
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Executors.newSingleThreadScheduledExecutor().execute(() -> getInstance(context).providerDao().insertAll(DatabaseSetup.setupDatabase()));
                                }
                            })
                            .addMigrations(MIGRATION_1_2)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract ProviderDao providerDao();
}
