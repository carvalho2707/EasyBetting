package pt.tiagocarvalho.easybetting.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ProviderDao {

    @Query("SELECT * FROM provider")
    List<Provider> getAll();

    @Query("SELECT * FROM provider where state = 1")
    List<Provider> getAllActive();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<Provider> providers);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Provider provider);

    @Query("SELECT * FROM provider WHERE name = :name")
    Provider getProviderByName(String name);
}
