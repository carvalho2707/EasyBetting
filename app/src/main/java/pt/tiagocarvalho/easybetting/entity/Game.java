package pt.tiagocarvalho.easybetting.entity;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Builder
public class Game {
    private String id;
    private String url;
    private String tournament;
    private String date;
    private String team1;
    private String team2;
    private String team1ImageUrl;
    private String team2ImageUrl;
    private BigDecimal ratio;
    private String odd1;
    private String odd2;
    private String provider1;
    private String provider2;

}
