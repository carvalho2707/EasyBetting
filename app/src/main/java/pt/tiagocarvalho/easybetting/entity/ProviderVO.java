package pt.tiagocarvalho.easybetting.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Builder
public class ProviderVO {
    private String name;
    private String url;
    private String imageUrl;
}
