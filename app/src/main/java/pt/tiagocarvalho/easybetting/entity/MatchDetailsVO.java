package pt.tiagocarvalho.easybetting.entity;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Builder
@Getter
@Setter
public class MatchDetailsVO {
    private String tournament;
    private String time;
    private String team1;
    private String team2;
    private String imageUrl1;
    private String imageUrl2;
    private BigDecimal ratio;
    private BigDecimal odd1;
    private BigDecimal odd2;
    private String provider1;
    private String provider2;
}
