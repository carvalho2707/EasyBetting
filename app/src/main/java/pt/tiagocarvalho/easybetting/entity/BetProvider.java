package pt.tiagocarvalho.easybetting.entity;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Builder
public class BetProvider {
    private BigDecimal odd1;
    private BigDecimal odd2;
    private String provider;

}
