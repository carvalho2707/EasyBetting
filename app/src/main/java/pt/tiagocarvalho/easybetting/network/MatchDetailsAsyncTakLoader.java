package pt.tiagocarvalho.easybetting.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import com.crashlytics.android.Crashlytics;

import pt.tiagocarvalho.easybetting.entity.MatchDetailsVO;
import pt.tiagocarvalho.easybetting.service.CsGoService;

public class MatchDetailsAsyncTakLoader extends AsyncTaskLoader<AsyncTaskResult<MatchDetailsVO>> {

    private final AsyncTaskLoaderListener mListener;
    private final String matchUrl;
    private final CsGoService csGoService = new CsGoService();
    private final boolean owned;

    public MatchDetailsAsyncTakLoader(@NonNull Context context, AsyncTaskLoaderListener asyncTaskLoaderListener, String matchUrl, boolean owned) {
        super(context);
        this.mListener = asyncTaskLoaderListener;
        this.matchUrl = matchUrl;
        this.owned = owned;
    }

    @Override
    protected void onStartLoading() {
        mListener.beforeExecute();
        forceLoad();
    }

    @Nullable
    @Override
    public AsyncTaskResult<MatchDetailsVO> loadInBackground() {
        AsyncTaskResult<MatchDetailsVO> asyncTaskResult = new AsyncTaskResult<>();
        try {
            MatchDetailsVO matchDetailsVO = csGoService.getMatchDetails(matchUrl, owned, getContext());
            asyncTaskResult.setResult(matchDetailsVO);
        } catch (Exception e) {
            Crashlytics.logException(e);
            asyncTaskResult.setError(e);
            stopLoading();
        }
        return asyncTaskResult;
    }
}
