package pt.tiagocarvalho.easybetting.network;

public interface AsyncTaskLoaderListener {
    void beforeExecute();
}
