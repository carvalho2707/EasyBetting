package pt.tiagocarvalho.easybetting.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import pt.tiagocarvalho.easybetting.data.EasyBettingDatabase;
import pt.tiagocarvalho.easybetting.data.Provider;

public class ProvidersAsyncTakLoader extends AsyncTaskLoader<AsyncTaskListResult<Provider>> {

    private final AsyncTaskLoaderListener mListener;

    public ProvidersAsyncTakLoader(@NonNull Context context, AsyncTaskLoaderListener asyncTaskLoaderListener) {
        super(context);
        this.mListener = asyncTaskLoaderListener;
    }

    @Override
    protected void onStartLoading() {
        mListener.beforeExecute();
        forceLoad();
    }

    @Nullable
    @Override
    public AsyncTaskListResult<Provider> loadInBackground() {
        AsyncTaskListResult<Provider> asyncTaskResult = new AsyncTaskListResult<>();
        try {
            EasyBettingDatabase db = EasyBettingDatabase.getInstance(getContext());
            List<Provider> result = db.providerDao().getAll();
            asyncTaskResult.setResult(result);
        } catch (Exception e) {
            Crashlytics.logException(e);
            asyncTaskResult.setError(e);
            e.printStackTrace();
            stopLoading();
        }
        return asyncTaskResult;
    }
}
