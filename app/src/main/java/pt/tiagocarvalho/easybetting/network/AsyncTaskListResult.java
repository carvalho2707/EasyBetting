package pt.tiagocarvalho.easybetting.network;

import java.util.List;

public class AsyncTaskListResult<T> {
    private List<T> result;
    private Exception error;

    public AsyncTaskListResult() {
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }
}
