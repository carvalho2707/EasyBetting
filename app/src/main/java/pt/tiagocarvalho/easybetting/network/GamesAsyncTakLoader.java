package pt.tiagocarvalho.easybetting.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import com.crashlytics.android.Crashlytics;

import java.util.Collections;
import java.util.List;

import pt.tiagocarvalho.easybetting.entity.Game;
import pt.tiagocarvalho.easybetting.service.CsGoService;
import pt.tiagocarvalho.easybetting.utils.GameComparator;

public class GamesAsyncTakLoader extends AsyncTaskLoader<AsyncTaskListResult<Game>> {

    private final AsyncTaskLoaderListener mListener;
    private final boolean winGuarantee;
    private final boolean today;
    private final boolean owned;
    private final int sortOrderId;
    private final CsGoService csGoService = new CsGoService();

    public GamesAsyncTakLoader(@NonNull Context context, AsyncTaskLoaderListener asyncTaskLoaderListener, boolean winGuarantee, boolean today, boolean owned, int sortOrderId) {
        super(context);
        this.mListener = asyncTaskLoaderListener;
        this.winGuarantee = winGuarantee;
        this.today = today;
        this.owned = owned;
        this.sortOrderId = sortOrderId;
    }

    @Override
    protected void onStartLoading() {
        mListener.beforeExecute();
        forceLoad();
    }

    @Nullable
    @Override
    public AsyncTaskListResult<Game> loadInBackground() {
        AsyncTaskListResult<Game> asyncTaskResult = new AsyncTaskListResult<>();
        try {
            List<Game> matches = csGoService.getMatches(winGuarantee, today, owned, getContext(), null);
            Collections.sort(matches, new GameComparator(sortOrderId));
            asyncTaskResult.setResult(matches);
        } catch (Exception e) {
            Crashlytics.logException(e);
            asyncTaskResult.setError(e);
            e.printStackTrace();
            stopLoading();
        }
        return asyncTaskResult;
    }
}
